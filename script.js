
// carousel-1

$(document).ready(function(){
    $('.carousel').slick({
        arrows: false,
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              infinite: true,
              dots: true
            }
          },
          {
            breakpoint: 800,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1
            }
          }
          
        ]
    });    
});

// carousel-2

$(document).ready(function(){
  $('.second-carousel').slick({
      
      arrows: false,
      dots: true,
      infinite: false,
      speed: 300,
      slidesToShow: 4,
      slidesToScroll: 4,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 800,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        
      ]
  });    
});


// carousel-tab-1

$(document).ready(function(){
  $('.single-item-tab-1').slick({
    
    dots: true,
    arrows: false
  });
});

//carousel-tab-3

$(document).ready(function(){
  $('.single-item-tab-3').slick({
    
    dots: true,
    arrows: false
  });
});

$(document).ready(function(){
  $('.powerPlant-carousel').slick({
      
      arrows: false,
      dots: true,
      infinite: false,
      speed: 300,
      slidesToShow: 4,
      slidesToScroll: 4,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 800,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
        
      ]
  });    
});

// chart-carousel

$(document).ready(function(){
  $('.chart-carousel').slick({
    dots: true,
    arrows: false,
    
  });
});

// 3d-carousel

const galleryContainer = document.querySelector('.gallery-container');
const galleryControlsContainer = document.querySelector('.gallery-controls');
const galleryControls = ['previous', 'next'];
const galleryItems = document.querySelectorAll('.gallery-item');

class Carousel {
  constructor(container, items, controls) {
    this.carouselContainer = container;
    this.carouselControls = controls;
    this.carouselArray = [...items];
  }

  // Assign initial css classes for gallery and nav items
  setInitialState() {
    
    this.carouselArray[0].classList.add('gallery-item-previous');
    this.carouselArray[1].classList.add('gallery-item-selected');
    this.carouselArray[2].classList.add('gallery-item-next');
    this.carouselArray[3].classList.add('gallery-item-last');

    
    document.querySelector('.gallery-nav').childNodes[0].className = 'gallery-nav-item gallery-item-previous';
    document.querySelector('.gallery-nav').childNodes[1].className = 'gallery-nav-item gallery-item-selected';
    document.querySelector('.gallery-nav').childNodes[2].className = 'gallery-nav-item gallery-item-next';
    document.querySelector('.gallery-nav').childNodes[3].className = 'gallery-nav-item gallery-item-last';
  }

  // Update the order state of the carousel with css classes
  setCurrentState(target, selected, previous, next, last) {     

    selected.forEach(el => {
      el.classList.remove('gallery-item-selected');

      if (target.className == 'gallery-controls-previous') {
        el.classList.add('gallery-item-next');
      } else {
        el.classList.add('gallery-item-previous');
      }
    });

    previous.forEach(el => {
      el.classList.remove('gallery-item-previous');

      if (target.className == 'gallery-controls-next') {
        el.classList.add('gallery-item-last');
      } else {
        el.classList.add('gallery-item-selected');//first
      }
    });

    last.forEach(el => {
      el.classList.remove('gallery-item-last');

      if (target.className == 'gallery-controls-previous') {
        el.classList.add('gallery-item-previous');
      } else {
        el.classList.add('gallery-item-next');
      }
    });


    next.forEach(el => {
      el.classList.remove('gallery-item-next');

      if (target.className == 'gallery-controls-previous') {
        el.classList.add('gallery-item-last'); //last
      } else {
        el.classList.add('gallery-item-selected');
      }
    });

    
    
  }

  // Construct the carousel navigation
  setNav() {
    galleryContainer.appendChild(document.createElement('ul')).className = 'gallery-nav';

    this.carouselArray.forEach(item => {
      const nav = galleryContainer.lastElementChild;
      nav.appendChild(document.createElement('li'));
    }); 
  }

  // Construct the carousel controls
  setControls() {
    this.carouselControls.forEach(control => {
      galleryControlsContainer.appendChild(document.createElement('button')).className = `gallery-controls-${control}`;
    }); 

    !!galleryControlsContainer.childNodes[0] ? galleryControlsContainer.childNodes[0].innerHTML = '<i class="fas fa-arrow-left fa-2x"></i>' : null;
    !!galleryControlsContainer.childNodes[1] ? galleryControlsContainer.childNodes[1].innerHTML = '<i class="fas fa-arrow-right fa-2x"></i>': null;
    
  }
 
  // Add a click event listener to trigger setCurrentState method to rearrange carousel
  useControls() {
    const triggers = [...galleryControlsContainer.childNodes];

    triggers.forEach(control => {
      control.addEventListener('click', () => {
        const target = control;
        const selectedItem = document.querySelectorAll('.gallery-item-selected');
        const previousSelectedItem = document.querySelectorAll('.gallery-item-previous');
        const nextSelectedItem = document.querySelectorAll('.gallery-item-next');
        const lastCarouselItem = document.querySelectorAll('.gallery-item-last');
        

        this.setCurrentState(target, selectedItem, previousSelectedItem, nextSelectedItem, lastCarouselItem);
      });
    });
  }
}

const exampleCarousel = new Carousel(galleryContainer, galleryItems, galleryControls);

exampleCarousel.setControls();
exampleCarousel.setNav();
exampleCarousel.setInitialState();
exampleCarousel.useControls();


// Power Plant Policy carousel

$(document).ready(function(){
  $('.power-plant-policy').slick({
    dots: false,
    arrows: true,
    infinite: true
    
  });
});


//modal

let modal = document.getElementById("modalCustom");

// Get the image 
let img = document.getElementById("buttonCustom");
let captionText = document.getElementById("caption");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}


//  close the modal
let span = document.getElementsByClassName("close")[0];

//  (x) > close the modal
span.onclick = function() {
  modal.style.display = "none";
}